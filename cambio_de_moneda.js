class Moneda{
    constructor(cantidad,USD){
        this.cantidad = cantidad;
        this.Dolar = USD;
    }
    precio_dolar = 19.81;

    cambiar(){
        if(this.Dolar===false){
            this.Dolar = true;
            this.cantidad = this.cantidad / this.precio_dolar;
        }
        else{
            this.Dolar = false;
            this.cantidad = this.cantidad * this.precio_dolar;
        }
        this.cantidad = this.cantidad.toFixed(2);
    }

    tipoDeMoneda(){
        if(this.Dolar === true){
            if(this.cantidad!=1){
                return "dolares";
            }
            else{
                return "dolar";
            }
        }
        else{
            if(this.cantidad!=1){
                return "pesos";
            }
            else{
                return "peso";
            }
        }
    }
} 

let moneda = new Moneda(50,false);
console.log('Tengo: ' + moneda.cantidad + ' ' + moneda.tipoDeMoneda());
console.log('Solicito cambiarlo a dolares');
moneda.cambiar();
console.log('Lo cambio a dolar y tengo');
console.log( moneda.cantidad + ' ' + moneda.tipoDeMoneda());
console.log('Solicito cambiarlo a dolares');
moneda.cambiar();
console.log('Ahora tengo: ' + moneda.cantidad + ' ' + moneda.tipoDeMoneda());
