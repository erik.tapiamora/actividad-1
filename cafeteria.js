class Producto{
    constructor(precio,cantidad, nombre,descuento){
        this.precio = precio;
        this.cantidad = cantidad;
        this.nombre = nombre;
        this.descuento = descuento;
    }
    
    total(){
        let descuento = (this.precio * this.cantidad) * this.descuento / 100;
        return this.precio * this.cantidad - descuento;
    }
} 

let  cafe = new Producto(50,2,'cafe',25);
let  late = new Producto(43,3,'late',25);
let  chocolate = new Producto(45,4,'chocolate',70);
////////////////////Proceso//////////////
let total = 0;
console.log('Se compraron 3 productos de los cuales: ');
console.log('Se pidio ' + cafe.cantidad + ' ' + cafe.nombre + ' que vale ' + cafe.precio + ' con un descuento del '+ cafe.descuento +'% con el cual fue un total de: ' + cafe.total());
productos.agregarProducto(cafe);
console.log('Se pidio ' + late.cantidad + ' ' + late.nombre + ' que vale ' + late.precio + ' con un descuento del '+ late.descuento +'% con el cual fue un total de: ' + late.total());
productos.agregarProducto(late);
console.log('Se pidio ' + chocolate.cantidad + ' ' + chocolate.nombre + ' que vale ' + chocolate.precio + ' con un descuento del '+ chocolate.descuento +'% con el cual fue un total de: ' + chocolate.total());
productos.agregarProducto(chocolate);

