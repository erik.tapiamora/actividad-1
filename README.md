# T01

Primer actividad

## Descripcion

En esta actividad se busca realizar un archivo de tipo de cambio de moneda
- [X] [Se subio el archivo de tipo de cambio de moneda]
- [X] [Se subio el archivo de manera erronea con un error]
- [X] [Se realizo un archivo de cambio de moneda correcto]
- [X] [Se tiene cambio de moneda dependiendo de si la moneda es peso o dolar]
- [X] [Se generó la clase de cafeteria que tiene productos con diversos descuentos y te da el descuento por producto]
- [X] [Se generó la clase de tienda que tiene productos con diversos descuentos y te da el descuento por producto y el total del listado completo]
