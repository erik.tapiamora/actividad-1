class Producto{
    constructor(precio,cantidad, nombre,descuento){
        this.precio = precio.toFixed(2);
        this.cantidad = cantidad;
        this.nombre = nombre;
        this.descuento = descuento;
    }
    
    total(){
        let descuento = (this.precio * this.cantidad) * this.descuento / 100;
        return (this.precio * this.cantidad - descuento).toFixed(2);
    }
} 

class Productos{
    constructor(){
        this.productos = [];
    }

    agregarProducto(producto){
        this.productos.push(producto);
    }


    obtenerTotal(){
        let total=0;
        this.productos.forEach(producto =>{
             const valorProducto = producto.total();
             total += parseInt(valorProducto);
            });
        return total;
    }

    obtenerCantidadProductos(){
        return this.productos.length;
    }

}

let carrito = new Productos();
carrito.agregarProducto(new Producto(50.00,5,'Jamón',15));
carrito.agregarProducto(new Producto(30.00,3,'Leche',30));
carrito.agregarProducto(new Producto(10.00,4,'Queso',0));
carrito.agregarProducto(new Producto(25.00,5,'Jabon',10));
carrito.agregarProducto(new Producto(45.00,6,'Cloro',50));

////////////////////Proceso/////////////////

console.log('Se compraron ' + carrito.obtenerCantidadProductos() + ' productos los cuales son: ');
console.log('# - Nombre - Precio - Cantidad - Descuento % - Total por producto');
carrito.productos.forEach((producto,key) => console.log((key+1)+ ' - ' + producto.nombre + ' - $' + producto.precio + ' - ' + producto.cantidad + ' - ' + producto.descuento + '% - $' + producto.total()) );
console.log('----------------------------------------------');
console.log('El total de la compra es de: $' + carrito.obtenerTotal());

